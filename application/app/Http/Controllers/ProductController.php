<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // read products.json file
        $products = json_decode(file_get_contents(public_path('products.json')), true);
        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // read products.json file
        $products = json_decode(file_get_contents(public_path('products.json')), true);

        // find existing product
        $newProduct = true;
        if ( count($products) > 0 ) {
            for ($i = 0; $i < count($products); $i++) {
                if (strtolower($products[$i]['name']) == strtolower($request['name'])) {
                    // found exisiting product
                    $newProduct = false;

                    // update existing product
                    $products[$i]['quantity'] = $request['quantity'];
                    $products[$i]['price'] = $request['price'];
                    $products[$i]['submitted_at'] = now();
                    $products[$i]['total_value_number'] = $products[$i]['quantity'] * $products[$i]['price'];
                } else {
                    continue;
                }
            }
        }

        // push new product to products array if $newProduct == true
        if ($newProduct) {
            $request['submitted_at'] = now();
            $request['total_value_number'] = $request['quantity'] * $request['price'];
            array_push($products, $request->all());
        }

        $prettyJSON = json_encode($products, JSON_PRETTY_PRINT);
        $status = file_put_contents(public_path('products.json'), stripslashes($prettyJSON));
        return response()->json($products);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
