<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Coalitions Technology Laravel Skills Test v1.0</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div class="container mt-5">
            <h2>
                Coalitions Technology Laravel Skills Test v1.0
            </h2>

            <hr>

            <div id="addProduct" class="my-5">
                <h3>
                    Add New Product
                </h3>
                <div>
                    Use the form below to add a new product
                </div>
                <br>

                <form id="addProductForm" method="post" action="javascript:;" class="mb-5">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="Product Name" minlength="2" />
                    </div>

                    <div class="form-group">
                        <label for="name">Quantity in Stock:</label>
                        <input class="form-control" type="number" name="quantity" id="quantity" placeholder="Product Quantity" min="0" />
                    </div>

                    <div class="form-group">
                        <label for="name">Price:</label>
                        <input class="form-control" type="number" name="price" id="price" placeholder="Product Price" min="0" step="any" />
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add Product</button>
                    </div>
                </form>
            </div>

            <hr>

            <div id="products" class="my-5">
                <h3>
                    Products Table
                </h3>
                <div>
                    Below is a List of all Added Products
                </div>
                <br>

                <div>
                    <table id="productsTable" class="table mb-5">
                        <caption id="productsOverview">There are <strong id="totalProducts">0</strong> Total Products!</caption>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Quantity in Stock</th>
                                <th>Price</th>
                                <th>Submitted At</th>
                                <th>Total Value Number</th>
                            </tr>
                        </thead>
                        <tbody id="productsTableBody">
                        </tbody>
                        <tfoot>
                            <tr class="font-weight-bold">
                                <td>&nbsp;</td>
                                <td>TOTAL</td>
                                <td id="totalQuantity">0</td>
                                <td id="totalPrice">0</td>
                                <td>&nbsp;</td>
                                <td id="totalValueNumber">0</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <hr>

            <div id="copyright" class="my-5">
                <p>
                    &copy; 2019 &middot; Tunji Oyeniran.
                </p>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                // Write JS Code Here

                // get and read products into products table
                getProducts();

                // addProductForm onSubmit
                $('#addProductForm').submit(function(e) {
                    e.preventDefault();

                    var formData = {};
                    formData.name = $('#name').val();
                    formData.quantity = $('#quantity').val();
                    formData.price = $('#price').val();

                    console.log(formData);
                    addProduct(formData);
                });
            });

            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

            function addProduct(data) {
                $.ajax({
                    url: '{{ url('product') }}',
                    type: 'POST',
                    data: data,
                })
                .done(function(data) {
                    readProductsIntoTable(data);
                    console.log("success");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            }

            function getProducts() {
                $.get('{{ url('product') }}', function(data) {
                    /*optional stuff to do after success */
                    readProductsIntoTable(data);
                    console.log('success');
                });
            }

            function readProductsIntoTable(products) {
                let totalProducts = products.length;
                let totalQuantity = 0;
                let totalPrice = 0;
                let totalValueNumber = 0;

                $('#productsTableBody').html("");
                for (var i = 0; i < products.length; i++) {
                    $('#productsTableBody').append('<tr>'+
                        '<td>'+ (i+1) +'</td>' +
                        '<td>'+ products[i]['name'] +'</td>' +
                        '<td>'+ products[i]['quantity'] +'</td>' +
                        '<td>'+ products[i]['price'] +'</td>' +
                        '<td>'+ new Date(products[i]['submitted_at']).toLocaleString() +'</td>' +
                        '<td>'+ products[i]['total_value_number'] +'</td>' +
                    '</tr>');

                    totalQuantity += Number(products[i]['quantity']);
                    totalPrice += Number(products[i]['price']);
                    totalValueNumber += Number(products[i]['total_value_number']);
                }

                $('#totalProducts').text(totalProducts);
                $('#totalQuantity').text(totalQuantity);
                $('#totalPrice').text(totalPrice);
                $('#totalValueNumber').text(totalValueNumber);
            }
        </script>
    </body>
</html>
