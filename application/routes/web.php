<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // create products.json file if it does not exist
    if (!file_exists(public_path('products.json'))) {
        $prettyJSON = json_encode(array(), JSON_PRETTY_PRINT);
        file_put_contents(public_path('products.json'), stripslashes($prettyJSON));
    }

    return view('product.index');
});

Route::resource('product', 'ProductController');
